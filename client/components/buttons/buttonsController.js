(function() {
  'use strict';

  angular
    .module('trsg.Controllers')
    .controller('buttonsController', buttonsController);

  buttonsController.$inject = [];

  function buttonsController() {
    var ctrl = this;

    var editor = CodeMirror.fromTextArea(document.getElementById('code'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });

    editor.setValue(
      '<button class="btn btn--small" type="button">small</button>\n\n' +
      '<button class="btn btn--regular" type="button">regular</button>\n\n' +
      '<button class="btn btn--large" type="button">large</button>\n\n' +
      '<button class="btn btn--regular btn--disabled" type="button" disabled>disabled</button>'
      );
  }
})();
