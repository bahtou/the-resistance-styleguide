(function() {
  'use strict';

  angular
    .module('trsg.Controllers')
    .controller('tabsController', tabsController);

  tabsController.$inject = [];

  function tabsController() {
    var ctrl = this;
    var activeTab = document.getElementsByClassName('tab__item--active')[0];
    var activePanel = document.getElementsByClassName('panel__item--active')[0];

    ctrl.selectTab = function($event) {
      $event.preventDefault();

      // debugger;
      var link = $event.target;
      var item = link.parentElement;
      var id = link.hash.replace('#', '');

      if (id && !item.classList.contains('tab__item--active')) {
        activePanel.classList.remove('panel__item--active');
        activeTab.classList.remove('tab__item--active');

        activeTab = link;
        activePanel = document.getElementById(id);

        activeTab.classList.add('tab__item--active');
        activePanel.classList.add('panel__item--active');
      }
    };

    ctrl.panel1 = function() {
      return '"This will begin to make things right. I\'ve traveled too far and seen too much to ignore the despair in the galaxy. Without the Jedi, there can be no balance in the force." — Lor San Tekka';
    };

    ctrl.panel2 = function() {
      return '"Beware of the dark side. Anger, fear, aggression; the dark side of the Force are they. Easily they flow, quick to join you in a fight. If once you start down the dark path, forever will it dominate your destiny, consume you it will." — Yoda';
    };

    ctrl.panel3 = function() {
      return '"An Ugly was any type of starfighter that had been cobbled together out of parts that had been salvaged from varying origins, including crashed starfighters and ex-military surplus." - <a href="http://starwars.wikia.com/wiki/Ugly">starwars wiki</a>';
    };

  }

})();
