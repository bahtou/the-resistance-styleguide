(function() {
  'use strict';

  angular
    .module('trsg.Controllers')
    .controller('gridsystemController', gridsystemController);

  gridsystemController.$inject = [];

  function gridsystemController() {
    var ctrl = this;

    var row12 = CodeMirror.fromTextArea(document.getElementById('row-12'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    row12.setValue(
      '<div class="grid__row">\n'+
      '  <div class="grid__cell --12">\n' +
      '    <div class="box">12 col</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var row4_8 = CodeMirror.fromTextArea(document.getElementById('row-4_8'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    row4_8.setValue(
      '<div class="grid__row">\n' +
      '  <div class="grid__cell --4">\n' +
      '    <div class="box">4 col</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell --8">\n' +
      '    <div class="box">8 col</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var row1_11 = CodeMirror.fromTextArea(document.getElementById('row-1_11'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    row1_11.setValue(
      '<div class="grid__row">\n' +
      '  <div class="grid__cell --1">\n' +
      '    <div class="box">1 col</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell --11">\n' +
      '    <div class="box">11 col</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var auto = CodeMirror.fromTextArea(document.getElementById('auto'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    auto.setValue(
      '<div class="grid__row">\n'+
      '  <div class="grid__cell auto">\n' +
      '    <div class="box">auto</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var auto_auto = CodeMirror.fromTextArea(document.getElementById('auto-auto'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    auto_auto.setValue(
      '<div class="grid__row">\n'+
      '  <div class="grid__cell auto">\n' +
      '    <div class="box">auto</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell auto">\n' +
      '    <div class="box">auto</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var auto_4col = CodeMirror.fromTextArea(document.getElementById('auto-4col'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    auto_4col.setValue(
      '<div class="grid__row">\n'+
      '  <div class="grid__cell --4">\n' +
      '    <div class="box">4 col</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell auto">\n' +
      '    <div class="box">auto</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var six_six = CodeMirror.fromTextArea(document.getElementById('six-six'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    six_six.setValue(
      '<div class="grid__row">\n'+
      '  <div class="grid__cell --6">\n' +
      '    <div class="box">6 col</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell --6">\n' +
      '    <div class="box">6 col</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var eight_four = CodeMirror.fromTextArea(document.getElementById('eight-four'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    eight_four.setValue(
      '<div class="grid__row">\n'+
      '  <div class="grid__cell --4">\n' +
      '    <div class="box">4 col</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell --4">\n' +
      '    <div class="box">4 col</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell --4">\n' +
      '    <div class="box">4 col</div>\n' +
      '  </div>\n' +
      '</div>'
    );

    var four_four_four = CodeMirror.fromTextArea(document.getElementById('four-four-four'), {
      lineNumbers: true,
      mode: 'text/html',
      readOnly: true
    });
    four_four_four.setValue(
      '<div class="grid__row">\n'+
      '  <div class="grid__cell --8">\n' +
      '    <div class="box">6 col</div>\n' +
      '  </div>\n' +
      '  <div class="grid__cell --4">\n' +
      '    <div class="box">6 col</div>\n' +
      '  </div>\n' +
      '</div>'
    );
  }
})();
