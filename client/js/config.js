(function() {
  'use strict';

  angular.module('trsg', [
    'trsg.routes',
    'trsg.Controllers'
  ]);

  angular.module('trsg.routes', ['ui.router']);
  angular.module('trsg.Controllers', []);

  angular
    .module('trsg')
    .run(function($rootScope) {
      $rootScope.$on('$stateChangeError', console.log.bind(console));
    });

})();
