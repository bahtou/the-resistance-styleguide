(
  function() {
    'use strict';

    function selectNavItem() {
      var navListItems = document.querySelectorAll('.nav-list__item a');

      for (var i = 0; i < navListItems.length; i++) {
        navListItems[i].addEventListener('click', handleNavEvent, false);
      }
    }

    function handleNavEvent(event) {
      //can also use 'this' instead of 'event.target'
      var currentUrl = window.location.href;
      var selected = document.querySelectorAll('.nav-list__item .selected')[0];
      var targetUrl = event.target.href;

      selected.classList.remove('selected');
      event.target.classList.add('selected');

      changeUrl(currentUrl, targetUrl);
      event.stopPropagation();
    }

    function changeUrl(currentUrl, targetUrl) {
      var modifiedUrl = currentUrl.split('#')[0];
      var startSlice = targetUrl.search(/\w*.html$/);
      var targetHash = targetUrl.slice(startSlice).split('.')[0];
      var nextPage;

      nextPage = modifiedUrl + '#' + targetHash;

      window.history.pushState({
        "html": '<h1>HOLA</h1>',
        "pageTitle": 'hello'},
        "",
        nextPage);
    }

    function onFirstPageLoadOrRefresh() {
      /*
        check if URL has a hashTag
        if it does navigate to where the hashTag is pointing
       */
      var currentPage = window.location.href;
      var navListItems = document.querySelectorAll('.nav-list__item a');
      var selectedNavItem = document.querySelectorAll('.nav-list__item .selected')[0];

      var hashName = '';
      var startHash;

      var targetNavItem;
      var startSlice;
      var hrefName;

      startHash = currentPage.search('#');

      if (startHash === -1) {
        selectedNavItem.click();
        return;
      }

      hashName = currentPage.slice(startHash + 1);

      for (var i=0; i < navListItems.length; i++) {
        startSlice = navListItems[i].href.search(/\w*.html$/);
        hrefName = navListItems[i].href.slice(startSlice).split('.')[0];

        if (hrefName === hashName) {
          targetNavItem = navListItems[i];
        }
      }

      selectedNavItem.classList.remove('selected');
      targetNavItem.classList.add('selected');

      targetNavItem.click();
    }

    function addListeners() {
      onFirstPageLoadOrRefresh();
      selectNavItem();
    }

    document.addEventListener('DOMContentLoaded', addListeners);
  }
)();
