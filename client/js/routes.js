(function() {
  'use strict';

  angular
    .module('trsg.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

  function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    //state list
    $stateProvider
      .state('layout', {
        url: '',
        abstract: true,
        views: {
          'header': {
            templateUrl: 'layout/header/header.html',
            controller: 'headerController',
            controllerAs: 'ctrl'
          },
          'nav': {
            templateUrl: 'layout/nav/nav.html',
            controller: 'navController',
            controllerAs: 'ctrl'
          },
          'footer': {
            templateUrl: 'layout/footer/footer.html',
            controller: 'footerController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('main', {
        parent: 'layout',
        url: '/',
        views: {
          'main@': {
            templateUrl: 'components/buttons/buttons.html',
            controller: 'buttonsController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('layout.buttons', {
        url: '/buttons',
        parent: 'layout',
        views: {
          'main@': {
            templateUrl: 'components/buttons/buttons.html',
            controller: 'buttonsController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('layout.gridsystem', {
        url: '/gridsystem',
        parent: 'layout',
        views: {
          'main@': {
            templateUrl: 'components/gridsystem/gridsystem.html',
            controller: 'gridsystemController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('layout.tables', {
        url: '/tables',
        parent: 'layout',
        views: {
          'main@': {
            templateUrl: 'components/table/table.html',
            controller: 'tableController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('layout.tabs', {
        url: '/tabs',
        parent: 'layout',
        views: {
          'main@': {
            templateUrl: 'components/tabs/tabs.html',
            controller: 'tabsController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('layout.layouts', {
        url: '/layouts',
        parent: 'layout',
        views: {
          'main@': {
            templateUrl: 'components/layouts/layouts.html',
            controller: 'layoutsController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('layout.cards', {
        url: '/cards',
        parent: 'layout',
        views: {
          'main@': {
            templateUrl: 'components/cards/cards.html',
            controller: 'cardsController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state('holy-grail', {
        url: '/layouts/holy-grail',
        views: {
          'main@': { templateUrl: 'components/layouts/holygrail.html'}
        }
      })
      .state('bloggy', {
        url: '/layouts/bloggy',
        templateUrl: 'components/layouts/bloggy.html'
      })
      .state('ecommerce', {
        url: '/layouts/ecommerce',
        templateUrl: 'components/layouts/ecommerce.html'
      });

    //fall through
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true,
      rewriteLinks: false
    });
  }

})();
