(function() {
  'use strict';

  angular
    .module('trsg.Controllers')
    .controller('navController', navController);

  navController.$inject = ['$location'];

  function navController($location) {
    var ctrl = this;

    initiateNavFromUrl();

    ctrl.navigate = function($event) {
      var link = $event.target;
      var elSelected = document.querySelector('a.selected.menu__link');

      if ($event.target.tagName !== 'A') {
        link = $event.target.parentElement;
      }

      if (!link.classList.contains('menu__link')) {
        return;
      }

      if (link.className === elSelected.className) {
        return;
      }

      swapSelectedClass(elSelected, link);
    };

    function initiateNavFromUrl() {
      var currentPath = $location.path().split('/')[1];
      var menuLinks = document.querySelectorAll('a.menu__link');
      var targetLink;

      if (!currentPath) {
        addSelectedClass(menuLinks[0]);
        return;
      }

      for (var i=0; i < menuLinks.length; i++) {
        targetLink = menuLinks[i].getAttribute('ui-sref');
        if (currentPath === targetLink.split('.')[1]) {
          addSelectedClass(menuLinks[i]);
        }
      }
    }
  }

  function addSelectedClass(addToElement) {
    addToElement.classList.add('selected');
  }

  function swapSelectedClass(removeFromElement, addToElement) {
    removeFromElement.classList.remove('selected');
    addToElement.classList.add('selected');
  }

})();
