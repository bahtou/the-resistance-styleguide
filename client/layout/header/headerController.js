(function() {
  'use strict';

  angular
    .module('trsg.Controllers')
    .controller('headerController', headerController);

  headerController.$inject = [];

  function headerController() {
    var ctrl = this;

    ctrl.myHead = 'controller says: I am the header controller';
  }
})();
